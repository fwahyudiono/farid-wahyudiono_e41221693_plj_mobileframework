//While-loop 1 looping angka 1-9 sederhana
void main(List<String> args) {
//   var flag = 1;
//   while (flag < 10) {
//     print("iterasi ke" + flag.toString());
//     flag++;
//   }
// }

//While-loop 2 looping mengembalikan angka total
//   var deret = 5;
//   var jumlah = 0;
//   while (deret > 0) {
//     jumlah += jumlah;
//     deret--;
//     print('jumlah saat ini:' + jumlah.toString());
//   }
//   print(jumlah);
// }

//For-loop 1 Looping angka 1-9 sederhana
//   for (var angka = 1; angka < 10; angka++) {
//     print('iterasi ke :' + angka.toString());
//   }
// }

//For-loop 2 Looping mengembalikan angka total
//   var jumlah = 0;
//   for (var deret = 5; deret > 0; deret--) {
//     jumlah += deret;
//     print('jumlah saat ini: ' + jumlah.toString());
//   }
//   print('jumlah terakhir: ' + jumlah.toString());
// }

//For-loop 3 Looping dengan increment dan decrement lebih dari 1
  for (var deret = 0; deret < 10; deret += 2) {
    print('iterasi dengan increment counter 2: ' + deret.toString());
  }
  print('---------------------');
  for (var deret = 15; deret > 0; deret -= 3) {
    print('iterasi dengan decrement counter : ' + deret.toString());
  }
}
