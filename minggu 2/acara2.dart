//Ternary
//import 'dart:math';

void main() {
//   var isThisWahyu = true;
//   if (isThisWahyu) {
//     print("wahyu");
//   } else {
//     print("bukan");
//   }
// }
//   var isThisFarid = true;
//   isThisFarid ? print("Farid") : print("bukan");
// }
// premis perbandingan suatu nilai
//   var mood = "happy";
//   if (mood == "happy") {
//     print("hari ini aku bahagia");
//   }
// }
//branching/percabangan sederhana
//   var minimarketStatus = "open";
//   if (minimarketStatus == "open") {
//     print("saya akan membeli telur dan buah");
//   } else {
//     print("minimarketnya tutup");
//   }
// }
//branching dengan kondisi
//   var minimarketStatus = "tutup";
//   var minuteRemainingToOpen = 5;
//   if (minimarketStatus == "tutup") {
//     print("saya akan membeli telur");
//   } else if (minuteRemainingToOpen >= 6) {
//     print("minimarket buka sebentar lagi, saya tungguin");
//   } else {
//     print("minimarket tutup, saya pulang lagi");
//   }
// }
//Kondisional bersarang
//   var minimarketStatus = "open";
//   var telur = "soldout";
//   var buah = "soldout";
//   if (minimarketStatus == "open") {
//     print("saya akan membeli telur dan buah");
//     if (telur == "soldout" || buah == "soldout") {
//       print("belanjaan saya tidak lengkap");
//     } else if (telur == "soldout") {
//       print("telur habis");
//     } else if (buah == "soldout") {
//       print("buah habis");
//     }
//   } else {
//     print("minimarket tutup, saya pulang lagi");
//   }
// }
//KOndisional dengan switch case
  var buttonPushed = 3;
  switch (buttonPushed) {
    case 1:
      {
        print('matikan TV!');
        break;
      }
    case 2:
      {
        print('turunkan volume TV');
        break;
      }
    case 3:
      {
        print('tingkatkan volume TV');
        break;
      }
    case 4:
      {
        print('matikan suara TV');
        break;
      }

    default:
      {
        print('tidak terjadi apa-apa');
      }
  }
}
